package org.sky.component.location;


public class LocationUtil {
	/**计算两点距离
	 * @param firstPoin
	 * @param secondPoin
	 * @return
	 */
	public static double  getDistance(LocationPoin firstPoin,LocationPoin secondPoin){
		double lon1,lat1,lon2,lat2;
		lon1=firstPoin.getLongitude();
		lat1=firstPoin.getLatitude();
		lon2=secondPoin.getLongitude();
		lat2=secondPoin.getLatitude();
		double R = 6371; 
        double distance = 0.0; 
        double dLat = (lat2 - lat1) * Math.PI / 180; 
        double dLon = (lon2 - lon1) * Math.PI / 180; 
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) 
                + Math.cos(lat1 * Math.PI / 180) 
                * Math.cos(lat2 * Math.PI / 180) * Math.sin(dLon / 2) 
                * Math.sin(dLon / 2); 
        distance = (2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))) * R; 
        return distance; 
	}
	
	public static void main(String[] args) {
		LocationPoin firstPoin=new LocationPoin();
		LocationPoin secondPoin= new LocationPoin();
		firstPoin.setLongitude(39.56);
		firstPoin.setLatitude(116.04);
		secondPoin.setLongitude(39.55);
		secondPoin.setLatitude(116.15);
		System.out.println(LocationUtil.getDistance(firstPoin, secondPoin));
		
	}
}
