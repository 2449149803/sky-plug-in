package org.sky.threadpool;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

public class MyThread3 {
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		ExecutorService executor =  Executors.newSingleThreadExecutor();
		FutureTask<String> future = new FutureTask<String>(
				new Callable<String>() {// 使用Callable接口作为构造参数
					public String call(){
						// 真正的任务在这里执行，这里的返回值类型为String，可以为任意类型
						try {
							Thread.sleep(100);
							System.out.println("OK");
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return "ok";
					}
				});
		executor.execute(future);
		System.out.println("执行额外任务");
		Thread.sleep(10000);
		System.out.println("等待线程结果"+future.get());
		System.out.println("线程结束");
		executor.shutdown();
	}
}
