package org.sky.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface TestService extends Remote{
	public String sysHello() throws RemoteException;
}
