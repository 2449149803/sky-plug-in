package com;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonTest {
	public static void main(String[] args) {
		String json = "{\"id\":\"1\"}";
		ExclusionStrategy myExclusionStrategy = new ExclusionStrategy() {
			@Override
			public boolean shouldSkipField(FieldAttributes fa) {
				return fa.getName().equals("id");
			}
			@Override
			public boolean shouldSkipClass(Class<?> clazz) {
				return false;
			}
		};
		Gson gson1 = new Gson();
		Gson gson2 = new GsonBuilder().setExclusionStrategies(
				myExclusionStrategy)
				.create();
		Person person = gson1.fromJson(json, Person.class);
		System.out.println(person.getId());
		System.out.println(gson2.toJson(person));
	}

	public class Person {
		private String id;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

	}
}
