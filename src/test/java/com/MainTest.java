package com;


import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.formula.functions.T;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.google.gson.Gson;


public class MainTest {
	//@Test
	public void test(){
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(
				"spring.xml");
	}
	
	//@Test
	public void test2(){
		List list = new ArrayList<T>();
		Map map = new HashMap();
		for (int i = 0; i < 10; i++) {
			map.put("id", i);
			map.put("name", i);
			list.add(map);
		}
		Gson gson=new Gson();
		System.out.println(gson.toJson(list));
	}
	
	//@Test
	public void test3(){
		StringBuilder sb = new StringBuilder();
		sb.append("00000344<?xml version=\"1.0\" encoding=\"gb2312\"?>"+"\n");  	   
        sb.append("<Message>"+"\n");  
        sb.append("<Head>"+"\n");  
        sb.append("<TranCode>auln07</TranCode>"+"\n");  
        sb.append("<OperNo>01001</OperNo>"+"\n");  
        sb.append("<BranchNo>01018</BranchNo>"+"\n");  
        sb.append("<servtp>EBK</servtp>"+"\n");  
        sb.append("<TransDate>20140508</TransDate>"+"\n");  
        sb.append("<NetBankSeqNo>0000112705</NetBankSeqNo>"+"\n");
        sb.append("</Head>"+"\n");          
        sb.append("<Body>"+"\n");  
        sb.append("<custno>1001699428</custno>"+"\n");  
        sb.append("<rwnmid>0</rwnmid>"+"\n");  
        sb.append("<rcrdnm>5</rcrdnm>"+"\n");  
        sb.append("</Body>"+"\n");  
        sb.append("</Message>"); 
        System.out.println(sb.toString());
		
	}
	
	//@Test
	public void TestMkdir(){		
		String str="G://1//3";
		File file = new File(str);
		file.mkdirs();
	}
	
	@Test
	public void TestStr(){
		String str= "0123.4";
		System.out.println(str.substring(str.indexOf("2"), str.indexOf(".")));
	}
	
	@Test
	public void TestInt(){
		Long i = Long.valueOf(Integer.MAX_VALUE);
		Long j = Long.valueOf(Integer.MAX_VALUE+1);
		Long k = Long.valueOf(Integer.MAX_VALUE+2);
		Long l = Long.valueOf(Integer.MAX_VALUE+3);
		System.out.println(i);
		System.out.println(j);
		System.out.println(k);
		System.out.println(l);
	}
	
	@Test
	public void TestFile(){
		File file = new File("E:\\oss\\cbd\\交接文档");
		for(File fileTmp:file.listFiles()){
			if(fileTmp.isDirectory()){
				System.out.println("dir:"+fileTmp.getName());
				getDirFile(fileTmp);
			}else{
				System.out.println(fileTmp.getName());
			}
		}
	}
	
	public void getDirFile(File file){
		for(File fileTemp : file.listFiles()){
			System.out.println(fileTemp.getName());
		}
	}
	
	@Test
	public void TestClass(){
		System.out.println(Object.class);
		System.out.println(Class.class);
		System.out.println(new Object().getClass());
		Map map = new HashMap();
		System.out.println(map.getClass().getClass());
	}
	
	@Test
	public void TestZero(){
		BigDecimal b1 = new BigDecimal(10/0.0);
		Double d = new Double(0.0);
		System.out.println(b1);
	}
}
