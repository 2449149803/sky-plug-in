package org.sky.prdcst.test;

import org.sky.prdcst.Producer;
import org.sky.prdcst.Product;
import org.sky.prdcst.Storage;

public class MyProducer extends Producer{
	
	public MyProducer(Storage s){
		this.s=s;
	}
	
	@Override
	public void run() {
		try {
			while(true){
				this.s.push(new MyProduct());
				Thread.sleep(100);
			}
		} catch (InterruptedException e) {

			e.printStackTrace();
		}
		
	}
	
	public class MyProduct extends Product{
		public MyProduct(){
			System.out.println("生产");
		}
	}
}
