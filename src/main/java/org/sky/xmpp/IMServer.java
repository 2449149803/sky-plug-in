package org.sky.xmpp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Message.Type;

public class IMServer { 
	    private static final String IP = "192.168.1.29";  
	    private static final Integer DK = 5222;  
	    private static Roster roster;  
	    private static String domain="test";
	  
	    /** 
	     * 获取连接 
	     *  
	     * @return connection 
	     */  
	    public static XMPPConnection getConnection() {  
	        ConnectionConfiguration config = new ConnectionConfiguration(IP, DK);  
	        XMPPConnection connection = new XMPPConnection(config);
	        config.setReconnectionAllowed(true);
			//config.setSendPresence(true);
			config.setSASLAuthenticationEnabled(true);
	        return connection;  
	    }
	    
	    /**注册用户
	     * @param account
	     * @param passWord
	     * @param con
	     * @return
	     */
	    public static Boolean createUser(String account,String passWord,XMPPConnection con){
	    	AccountManager amgr = con.getAccountManager();
	    	try {
				amgr.createAccount(account,passWord);
				return true;
			} catch (XMPPException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
	    }
	    
	    /**群发消息
	     * @param message
	     */
	    public static void sendPubMsg(String message,XMPPConnection con){
	    	Message m = new Message();  
	        m.setBody(message);// 设置消息。  
	        m.setTo("all@broadcast."+domain);//[groupname]@[serviceName].[serverName]  
	        con.sendPacket(m);  
	    }
	    
	    /**发送即时消息
	     * @param message
	     * @param toUser
	     * @param con
	     */
	    public static void sendImMsg(String messageStr,String toUser,XMPPConnection con){
	    	Message message = new Message(toUser+"@"+domain, Type.chat);
            message.setBody(messageStr);
            con.sendPacket(message);
	    }
	    /** 
	     * 发送即时消息 ，独立窗口
	     *  
	     * @param username 
	     * @param pass 
	     * @param messgage 
	     * @throws XMPPException 
	     */  
	    public void SendMsg(String username, String pass, String messgage)  
	            throws XMPPException {  
	    	XMPPConnection con = IMServer.getConnection();  
	        con.connect();  
	        con.login(username, pass);  
	        Chat mychat = con.getChatManager().createChat("admin@"+domain, // 接收端的JID，JID是要加域的  
	                new MessageListener() {  
	                    @Override  
	                    public void processMessage(Chat chat, Message message) {  
	                        String messageBody = message.getBody();  
	                        System.out.println("收到信息：" + messageBody + " "  
	                                + message.getFrom());  
	                    }  
	                });  
	        mychat.sendMessage(messgage); // 发送信息  
	        con.disconnect(); // 断开连接  
	    }  
	  
	    /** 
	     * 获取好友列表 
	     *  
	     * @param username 
	     * @param pass 
	     * @return 
	     * @throws XMPPException 
	     */  
	    public List<RosterEntry> getRosterList(String username, String pass)  
	            throws XMPPException {  
	    	XMPPConnection con = IMServer.getConnection();  
	        con.connect();  
	        con.login(username, pass);  
	        Collection<RosterEntry> rosters = con.getRoster().getEntries();  
	        for (RosterEntry rosterEntry : rosters) {  
	            System.out.print("name: " + rosterEntry.getName() + ",jid: "  
	                    + rosterEntry.getUser()); // 此处可获取用户JID  
	            System.out.println("");  
	        }  
	        return null;  
	    }  
	  
	    /** 
	     * 获取用户列表（含组信息） 
	     *  
	     * @param username 
	     * @param pass 
	     * @return 
	     * @throws XMPPException 
	     */  
	    public List<RosterEntry> getRoster(String username, String pass)  
	            throws XMPPException {  
	    	XMPPConnection con = IMServer.getConnection();  
	        con.connect();  
	        con.login(username, pass);  
	        roster = con.getRoster();  
	        List<RosterEntry> EntriesList = new ArrayList<RosterEntry>();  
	        Collection<RosterEntry> rosterEntry = roster.getEntries();  
	        Iterator<RosterEntry> i = rosterEntry.iterator();  
	        while (i.hasNext()) {  
	            EntriesList.add(i.next());  
	        }  
	        return EntriesList;  
	    }  
	   
}
