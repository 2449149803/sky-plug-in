package org.sky.mongodb.dao;

import java.util.List;

import org.sky.mongodb.dao.entity.MPO;
import org.sky.mongodb.dao.entity.Page;
import org.springframework.data.mongodb.core.query.Query;

public interface IMongoDBDao {
	public <T extends MPO> void insert(T e, String collectionName);
	
	public long findCount(String collectionName);
	
	public <T extends MPO> List<T> getAll(String collcetionName,Class<T> clazz);
	
	public <T extends MPO> T findByID(String collcetionName,Class<T> clazz,String id);
	
	public <T extends MPO> T findOne(String collcetionName,Class<T> clazz,Query query);
	
	public <T extends MPO> List<T> find(Query query,String collcetionName,Class<T> clazz);
	
	public <T extends MPO> List<T> findByPage(String collcetionName,Class<T> clazz,Page page);
	
	public Boolean removeAll(String collcetionName);
	
	public Boolean removeById(String collcetionName,String id);
	
	public <T extends MPO> Boolean update(String collectionName ,T t, Class<T> clazz);

}
