package org.sky.xmpp;

import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.xmpp.packet.JID;

public class TestClient2 {
	public static void main(String[] args) throws XMPPException {
		//获取连接
		XMPPConnection con = IMUtil.getConnection();
		con.connect();     
        //登陆
		String email = JID.escapeNode("test4");
		con.login(email, "test4");     
        System.out.println("Authenticated = " + con.isAuthenticated());
        //注册用户
//        Boolean bool = IMServer.createUser("test3", "test3", con);
//        System.out.println(bool);
        //群发消息
        IMUtil.sendPubMsg("群发消息", con);
        //发送即时消息
        IMUtil.sendImMsg("即时消息", "test@test.com", con);
	}
}
