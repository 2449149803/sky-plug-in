package com;

public class StaticTest {
	public static void main(String[] args) {
		final Table s = new Table();
		new Call(){
			@Override
			public Table callBack() {
				s.name="cc";
				return s;
			}
		}.callBack();
		
		System.out.println(s.name);
	}
	
	public interface Call{
		public Table callBack();
	}
	
	public static class Table{
		public String name="BB";
	}
}
