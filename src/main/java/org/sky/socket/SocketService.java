package org.sky.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import org.sky.util.PropertieRead;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//@Component
public class SocketService extends Thread {
	private Logger log =LoggerFactory.getLogger(SocketService.class);
	// 当前的连接数
	static int socketConnect = 0;
	private ServerSocket serverSocket;
	// 服务器端口
	private int port = 8888;
	//扫描间隔  
    private int scanTime = 1000;
    public SocketService(){
    	this.start();
    }
    public void run(){
    	try{
    		PropertieRead pr=new PropertieRead(null);
    		if(pr.getProperty("headkeepport")!=null || pr.getProperty("headkeepport")!=""){
    			port=Integer.parseInt(pr.getProperty("headkeepport"));
    		}
	    	serverSocket = new ServerSocket(port);  
	        if(serverSocket == null){  
	            log.info("创建ServerSocket失败");  
	            return;  
	        }
	        log.info("创建ServerSocket成功");  
			while (true) {
				Socket socket = null;
				// 接收客户端的连接
				socket = serverSocket.accept();
				socketConnect = socketConnect + 1;
				log.info("当前连接数:"+socketConnect);
				// 为该连接创建一个工作线程
				Thread workThread = new Thread(new Handler(socket));
				// 启动工作线程
				workThread.start();
			}  
    	}catch(Exception e){
    		
    	}
    }
  //工作线程类  
    class Handler implements Runnable{  
        private Socket socket;  
        /** 
         * 构造函数，从调用者那里取得socket 
         * @param socket 指定的socket 
         * @author dream 
        */  
        public Handler(Socket socket){  
            this.socket = socket;  
        }  
          
        /** 
         * 从指定的socket中得到输入流 
         * @param socket 指定的socket 
         * @return 返回BufferedReader 
         * @author dream 
         */  
        private BufferedReader getReader(Socket socket){  
            InputStream is = null;  
            BufferedReader br = null;  
  
            try {  
                is = socket.getInputStream();  
                br = new BufferedReader(new InputStreamReader(is));  
            } catch (IOException e) {  
                e.printStackTrace();  
            }  
            return br;  
        }  
          
        public void run() {  
            try{  
            	log.info(socket.getInetAddress()+":"+socket.getPort());  
                BufferedReader br = getReader(socket);  
                String meg = null;  
                while ((meg = br.readLine()) != null) {  
                	log.info("head pack:"+meg);
                }  
            }catch(IOException e){  
            	
            }finally{  
                if(socket != null){  
                    try {  
                        //断开连接  
                        socket.close();
                        socketConnect--;
                    	log.info(socket.getInetAddress()+"断开连接");
                    	log.info("当前连接数:"+socketConnect);
                    } catch (IOException e) {  
                        e.printStackTrace();  
                    }  
                }  
            }  
        }
    }
}
