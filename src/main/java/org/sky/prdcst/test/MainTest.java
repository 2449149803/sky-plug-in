package org.sky.prdcst.test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.sky.prdcst.Storage;

public class MainTest {
	public static void main(String[] args) {
	
		Storage s = new Storage(1000);

		ExecutorService service = Executors.newCachedThreadPool();
		MyProducer mp = new MyProducer(s);
		MyConsumer mc = new MyConsumer(s);
		service.submit(mp);
		service.submit(mc);
	}
}
