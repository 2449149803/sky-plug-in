package com;




public class TryTest {
	private static int i=0;
	public static int t(){
		try{
			//throw new Exception();
			return 1;
		}catch(Exception e){
			return 2;
		}finally{
			++i;
			//return i;
		}
	}
	public static void main(String[] args) {
		int a=t();
		System.out.println(a);
		System.out.println(i);
		//这个输出结果说明finally永远执行，但是return也是有作用的。
	}
}
