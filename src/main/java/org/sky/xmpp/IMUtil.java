package org.sky.xmpp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Message.Type;

public class IMUtil {
		private static final String IP = "192.168.1.29";
		private static final Integer DK = 5222;
		private static Roster roster;
		private static String domain = "test";
	  
	    /** 
	     * 获取连接 
	     *  
	     * @return connection 
	     */  
	    public static XMPPConnection getConnection() {  
	        ConnectionConfiguration config = new ConnectionConfiguration(IP, DK);  
	        XMPPConnection connection = new XMPPConnection(config);  
	        return connection;  
	    }
	    
	    /**注册用户
	     * @param account
	     * @param passWord
	     * @param con
	     * @return
	     */
	    public static Boolean createUser(String account,String passWord,XMPPConnection con){
	    	AccountManager amgr = con.getAccountManager();
	    	try {
				amgr.createAccount(account,passWord);
				return true;
			} catch (XMPPException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
	    }
	    
	    /**群发消息
	     * @param message
	     */
	    public static void sendPubMsg(String message,XMPPConnection con){
	    	Message m = new Message();  
	        m.setBody(message);// 设置消息。  
	        m.setTo("all@broadcast."+domain);//[groupname]@[serviceName].[serverName]  
	        con.sendPacket(m);  
	    }
	    
	    /**发送即时消息
	     * @param message
	     * @param toUser
	     * @param con
	     */
	    public static void sendImMsg(String messageStr,String toUser,XMPPConnection con){
	    	Message message = new Message(toUser+"@"+domain, Type.chat);
         message.setBody(messageStr);
         con.sendPacket(message);
	    }
}
