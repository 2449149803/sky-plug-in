package org.sky.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class ScriptUtil {
	/**
	 * 调用bat命令
	 */
	public static void callCmd(String locationCmd){ 
		try {
	          Process child = Runtime.getRuntime().exec("cmd.exe /C start "+locationCmd);
	          InputStream in = child.getInputStream();
	          int c;
	          while ((c = in.read()) != -1) {
	        	  
	          }
	          in.close();
	          child.waitFor();
	          System.out.println("ok");
	     } catch (IOException e) {
	    	   System.err.println("processes was interrupted");
	           e.printStackTrace();
	     } catch (InterruptedException e) {
	    	System.err.println("processes was interrupted");
			e.printStackTrace();
		}
	 }
	
	 /**调用shell脚本
	 * @throws IOException
	 */
	public static void callShell(String locationCmd) throws IOException{
		 
		  Runtime rt=Runtime.getRuntime();
		  
		  String command=locationCmd;
		  
		  Process pcs=rt.exec(command);
		  
		  BufferedReader br = new BufferedReader(new InputStreamReader(pcs.getInputStream()));
		  String line=new String();
		  try{
			  while((line = br.readLine()) != null)
			  {
				  
			  }
			  pcs.waitFor();
		  }
		  catch(InterruptedException e){
			  System.err.println("processes was interrupted");
		  }finally{
			  br.close();
		  }
		  
		  int ret=pcs.exitValue();
		  System.out.println(ret);
		  System.out.println("执行完毕!");
	 }
	
	  
	
	public static void main(String[] args) throws IOException {
		 //callCmd("D:/run.bat");
		callShell("/export/home/xlg/solarischk.sh");
	}
}
