package com;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileTest {
	public static void main(String[] args) throws IOException, InterruptedException {
		File file = new File("D://text.txt");
		final OutputStream os = new FileOutputStream(file);
		final Long time = System.currentTimeMillis();
		Runnable r = new Runnable(){
			@Override
			public void run() {
				while(System.currentTimeMillis()-time<10000){
					try {
						System.out.println(Thread.currentThread().getName());
						os.write("hello world\n".getBytes());
					} catch (IOException e) {

					}
				}
			}
		};
	
		for(int i=0;i<1;i++){
			Thread t = new Thread(r,String.valueOf(i));
			t.start();
		}
		Thread.sleep(1000);
		os.write("hello world2\n".getBytes());
	}
	
}
