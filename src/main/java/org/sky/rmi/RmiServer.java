package org.sky.rmi;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class RmiServer {
	public static void main(String[] args) throws RemoteException, MalformedURLException {
		TestService test = new TestServiceImpl();
		LocateRegistry.createRegistry(6600);  
        Naming.rebind("rmi://127.0.0.1:6600/test", test);  
	}
}
