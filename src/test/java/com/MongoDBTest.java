package com;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.sky.mongodb.dao.IMongoDBDao;
import org.sky.mongodb.dao.entity.MPO;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

public class MongoDBTest {
	@Test
	public void MongoDBTest(){
		 ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "classpath:spring-mongodb.cfg.xml" });
		 IMongoDBDao m = (IMongoDBDao) context.getBean("mongoDBDao");
		 //m.removeAll("UserInfo");
		 UserInfo u = new UserInfo();
		 u.setName("caishaodong");
		 u.setAge("24");
		 u.setJob("IT");
		 m.insert(u, "UserInfo");
		 UserInfo u2 = m.findOne("UserInfo", UserInfo.class, new Query().addCriteria(new Criteria("name").is("caishaodong")));
		 System.out.println(u2.getName());
		 u2.setName("caishaodong");
		 u2.setAge("20");
		 m.update("UserInfo",u2, UserInfo.class);
		 System.out.println(u2.getId());
		 //m.removeById("UserInfo",u2.getId());
	}
	
	public class UserInfo implements MPO{
		private String id;
		private String name;
		private String age;
		private String job;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getAge() {
			return age;
		}
		public void setAge(String age) {
			this.age = age;
		}
		public String getJob() {
			return job;
		}
		public void setJob(String job) {
			this.job = job;
		}
		
	}
}
