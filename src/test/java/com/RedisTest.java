package com;

import org.junit.Test;
import org.sky.redis.dao.IRedisDao;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class RedisTest{
	@Test
	public void redisTest() throws InterruptedException{
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "classpath:spring-redis.cfg.xml" });
		IRedisDao redisDao = (IRedisDao) context.getBean("redisDao");
		redisDao.sendMessage("def", "java");
		while(true){
			redisDao.sendMessage("def", "java");
			Thread.sleep(1000);
		}
	}
	
	@Test
	public void redisTest2() throws InterruptedException{
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "classpath:spring-redis.cfg.xml" });
		IRedisDao redisDao = (IRedisDao) context.getBean("redisDao");
		redisDao.getMessage("def");
	}
	
}
