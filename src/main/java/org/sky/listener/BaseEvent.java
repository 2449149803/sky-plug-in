package org.sky.listener;

import java.util.EventObject;

public class BaseEvent extends EventObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1554867590273059892L;
	
	public BaseEvent(Object source) {
		super(source);
	}
	
}
