package org.sky.rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class TestServiceImpl extends UnicastRemoteObject  implements TestService{

	protected TestServiceImpl() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1564311414005156284L;

	@Override
	public String sysHello() {
		System.out.println("hello");
		return "hello";
	}
	
}
