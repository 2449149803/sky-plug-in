package com;

import javax.jms.Destination;

import org.junit.Test;
import org.sky.mq.ProducerService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MQTest {
	@Test
	public void test() {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				new String[] { "classpath:spring-mq.cfg.xml" });
		ProducerService producerService = context.getBean(ProducerService.class);
		Destination destination =  (Destination) context.getBean("topicDestination");
		for (int i = 0; i < 2; i++) {
			producerService.sendMessage(destination, "你好，生产者！这是消息：" + (i + 1));
		}
		while(true){
			
		}
	}
	@Test
	public void test2(){
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
				new String[] { "classpath:spring-mq-ltn.cfg.xml" });
		while(true){
			
		}
	}
}
