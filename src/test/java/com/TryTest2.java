package com;



public class TryTest2 {
	private static int i=0;
	public static int t(){
		try{
			return i;
		}catch(Exception e){
		}finally{
			++i;
			return i;
		}
		
	}
	public static void main(String[] args) {
		int a=t();
		System.out.println(a);
		System.out.println(i);
		//这个输出结果说明finally永远执行，两个return带有优先级，finally的优先级比较高。
	}
}
