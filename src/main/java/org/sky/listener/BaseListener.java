package org.sky.listener;

import java.util.EventListener;

public interface BaseListener extends EventListener {
	public void doEvent(BaseEvent be);
}
