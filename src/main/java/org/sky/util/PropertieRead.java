package org.sky.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertieRead {
	private  Properties p;

	/**path 默认路径config.properties
	 * @param path
	 */
	public PropertieRead(String path){
		//config.properties
		try{
			InputStream inputStream=null;
			if(path!=null){
				inputStream = PropertieRead.class.getClassLoader().getResourceAsStream(path);
			}else{
				inputStream = PropertieRead.class.getClassLoader().getResourceAsStream("config.properties");
			}
			    
			p = new Properties();    
			p.load(inputStream);
		}catch(Exception e){
			
		}
	}
	public String getProperty(String key){
		String value="";
		try{
			value=p.getProperty(key);
		}catch(Exception e){
			
		}
			return value;
	}
	public static void main(String[] args) throws IOException {
		PropertieRead pr=new PropertieRead("config.properties");
		System.out.println(pr.getProperty("serviceport"));
	}
}
