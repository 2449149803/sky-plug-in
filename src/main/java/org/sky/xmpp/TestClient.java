package org.sky.xmpp;

import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.sky.xmpp.handler.MessageHandler;
import org.sky.xmpp.listener.MyMsgListener;

public class TestClient {
	public static void main(String[] args) throws XMPPException, InterruptedException {
		//获取连接
		XMPPConnection con = IMServer.getConnection();
		try{
		con.connect();     
        //登陆
		con.login("test", "test");     
        System.out.println("Authenticated = " + con.isAuthenticated());
        //注册用户
//        String email = JID.escapeNode("test4@test.com");
//        Boolean bool = IMServer.createUser(email, "test4", con);
//        System.out.println(bool);
        //群发消息
        //IMServer.sendPubMsg("群发消息", con);
        //发送即时消息
        //IMServer.sendImMsg("即时消息", "test2", con);
        MyMsgListener myMsgListener = new MyMsgListener(con,new MessageHandler());
        }catch(Exception e){
        	
        }finally{
        	Thread.sleep(3600000);     
            con.disconnect();  
        }
	}
}
