package org.sky.threadpool;

import javax.transaction.Synchronization;


public class MyThread implements Runnable {
	private Object object;
	private int flag;
	private MyThread2 thread;
	public MyThread(Object object,int flag,MyThread2 thread){
		this.object=object;
		this.flag=flag;
		this.thread=thread;
	}
	public MyThread(){
		
	}
	@Override
	public void run() {
//		synchronized (object){
//			for (int i = 0; i < 5; i++) {
//				System.out.println(Thread.currentThread().getName()
//						+ " synchronized loop " + i);
//			}
//		}
		
		try {
			System.out.println(thread.getState());
			thread.join();
			System.out.println(thread.getState());
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("ok");
	}
	
	public static void main(String[] args) throws InterruptedException {
		Object object = new Object();
		Object object2 = new Object();
		MyThread2 myThread2 = new MyThread2();
		MyThread myThread = new MyThread(object,0,myThread2);
		Thread thread = new Thread(myThread, "1");
		thread.start();
		myThread2.start();
		System.out.println(thread.getState());
	}
	
	
}
