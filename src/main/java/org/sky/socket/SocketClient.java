package org.sky.socket;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class SocketClient {
	
	private Socket s = null;
	private BufferedReader in = null;
	private PrintWriter out = null;
	
	public SocketClient(String host,int port) {
		try {
			s = new Socket(host, port);
			System.out.println("Connected!");
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(s.getOutputStream())), true);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		SocketClient c = new SocketClient("127.0.0.1",8000);

		Scanner input = new Scanner(System.in);
		  
		String info = input.nextLine();
	   
		c.out.println(info);
		
		c.close();
		
	}
	public void close() throws IOException{
		s.close();
		in.close();
		out.close();
	}
}
