package org.sky.listener;

import java.util.ArrayList;
import java.util.List;

public class BaseSource {
	private List repository = new ArrayList<BaseListener>();

	// 注册监听器，如果这里没有使用Vector而是使用ArrayList那么要注意同步问题
	public void addDemoListener(BaseListener dl) {
		repository.add(dl);// 这步要注意同步问题
	}

	// 如果这里没有使用Vector而是使用ArrayList那么要注意同步问题
	public void notifyDemoEvent(BaseEvent event) {
		for (int i = 0; i < repository.size(); i++) {
			BaseListener dl = (BaseListener) repository.get(i);
			dl.doEvent(event);
		}
	}

	// 删除监听器，如果这里没有使用Vector而是使用ArrayList那么要注意同步问题
	public void removeDemoListener(BaseListener dl) {
		repository.remove(dl);// 这步要注意同步问题
	}

}
