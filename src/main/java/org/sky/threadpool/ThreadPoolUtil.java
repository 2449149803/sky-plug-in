package org.sky.threadpool;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.poi.ss.formula.functions.T;

/**线程工具
 * @author 蔡少东
 *
 */
public class ThreadPoolUtil {
	
	/**获取线程池
	 * @param num
	 * @return
	 */
	@SuppressWarnings("null")
	public ExecutorService getExecutorService(Integer num){
		if(num==null){
			return Executors.newFixedThreadPool(num);
		}else{
			return Executors.newFixedThreadPool(20);
		}
	}
	
	/**
	 * 异步执行任务
	 */
	public void asynExecute(final List<T> list){
		ExecutorService executorService = getExecutorService(10);
		for (int i=0 ;i<list.size();i++) {
			
		}
		executorService.shutdown();
	}
	
	public static void main(String[] args) {
		List<Integer> list = new ArrayList();
		for (int i = 0; i < 100000; i++) {
			list.add(i);
		}
		ThreadPoolUtil t = new ThreadPoolUtil();
	}
	
	
}
