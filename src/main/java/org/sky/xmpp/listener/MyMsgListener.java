package org.sky.xmpp.listener;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.sky.xmpp.handler.BaseHandler;

public class MyMsgListener {
	public MyMsgListener(XMPPConnection con,final BaseHandler handler){
		PacketFilter filterMessage = new PacketTypeFilter(Message.class);     
	    
        PacketListener myListener = new PacketListener() {     
            public void processPacket(Packet packet) {     
            	handler.proess(packet);
            }     
        };     
        // register the listener to the connection     
        con.addPacketListener(myListener, filterMessage); 
	}
}
