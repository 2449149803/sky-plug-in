package org.sky.rmi;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

public class RmiClient {
	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException {
		TestService test = (TestService) Naming.lookup("rmi://127.0.0.1:6600/test");
		System.out.println(test.sysHello());
	}
}
